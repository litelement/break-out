Break-out LitElement tutorial
=============================

We are going to develop a very simple version of the break-out clasic game using LitElement.
Do you rembember?

```
 === === === === === === === ===
 === === === === === === === ===
 === === ===     === === === ===
 === === ===         === === ===
               o 




            <--->
________________________________
```
Links and Tools
===============
lit-element: https://lit-element.polymer-project.org/
open-wc: https://open-wc.org/index.html

Visual Studio code: https://code.visualstudio.com/
  Recomended extensions: lit-html, lit-plugin

Step 01
====================

Get Started
-----------

First of all, lets create a quick app scaffold for our game. 
Just follow these simple steps and we will have a working app in few minutes.

```ssh
npm init @open-wc
```

And select:
✔ What would you like to do today? › Scaffold a new project
✔ What would you like to scaffold? › Application
✔ What would you like to add? › (choose whatever extras you want)
✔ Would you like to use typescript? › No
✔ What is the tag name of your application/web component? … break-out-game
✔ Do you want to write this file structure to disk? › Yes
✔ Do you want to install dependencies? › Yes, with npm

This will create the following structure to disk and install all dependencies we will need.

```
./
├── break-out-game/
│   ├── src/
│   │   ├── break-out-game.js
│   │   ├── BreakOutGame.js
│   │   └── open-wc-logo.js
│   ├── .editorconfig
│   ├── .gitignore
│   ├── custom-elements.json
│   ├── index.html
│   ├── LICENSE
│   ├── package.json
│   └── README.md
```

Enter ``break-out-game`` folder, and lets see what we have got.
```
cd break-out-game
npm run start
```

My first properties
-------------------
Modify **BreakOutGame** component to have three properties:
 - title => String, default value empty string.
 - subtitle => String, default value empty string.
 - author => String, default value empty string.

Modify **index.html** to set values to the properties:
 - title = "BreakOut Tutorial"
 - subtitle = "A lit-element remake"
 - author = "Fernando García Huerta"

The result should be something like this:
![Result](./result.png "Step 01")
